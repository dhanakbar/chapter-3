import React from 'react'
import style from './Button.module.css'

function Button() {
    return (
        <div className={style.button_cta}>
            Let's Explore
        </div>
    )
}

export default Button
